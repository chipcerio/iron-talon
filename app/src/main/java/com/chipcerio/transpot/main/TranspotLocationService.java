package com.chipcerio.transpot.main;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.chipcerio.transpot.R;
import com.chipcerio.transpot.main.MainActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class TranspotLocationService extends Service implements ConnectionCallbacks,
        OnConnectionFailedListener, LocationListener {
    // http://stackoverflow.com/a/17861235/1076574

    private static final String TAG = "TranspotLocationService";

    private static final String ACTION_STOP_GPS = "com.chipcerio.transpot.GPS_SERVICE";

    private static final int NOTIFY_ID = 101;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private NotificationManager mNotifyManager;

    @Override
    public void onCreate() {
        super.onCreate();
        buildGoogleApiClient();
        createLocationRequest();
        registerReceiver(mStopGpsServiceReceiver, new IntentFilter(ACTION_STOP_GPS));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mGoogleApiClient.connect();
        Log.d(TAG, "GoogleApiClient connected!");
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
            Log.d(TAG, "GoogleApiClient disconnected!");
        }
        unregisterReceiver(mStopGpsServiceReceiver);
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        // TODO: 21/01/2017 change gps interval to 5mins
        mLocationRequest.setInterval(2000); // 300000 millis == 5 mins
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "onConnected");
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        startStickyNotification();
    }

    private void startStickyNotification() {
        Intent mainIntent = new Intent(this, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntent(mainIntent);
        // Gets a PendingIntent containing the entire back stack
        PendingIntent mainPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        // http://stackoverflow.com/a/9241955/1076574
        PendingIntent stopServicePendingIntent = PendingIntent.getBroadcast(
                this, 0, new Intent(ACTION_STOP_GPS), PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(getString(R.string.text_app_run_in_background))
                        .addAction(R.drawable.ic_clear_black_36dp, getString(R.string.close),
                                stopServicePendingIntent)
                        .setAutoCancel(false)
                        .setLocalOnly(true)
                        .setOngoing(true)
                        .setPriority(NotificationCompat.PRIORITY_MAX);
        builder.setContentIntent(mainPendingIntent);
        mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotifyManager.notify(NOTIFY_ID, builder.build());
    }

    private BroadcastReceiver mStopGpsServiceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "receiving a stop gps broadcast");
            mNotifyManager.cancel(NOTIFY_ID);
            stopSelf();
        }
    };

    @Override
    public void onConnectionSuspended(int cause) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location loc) {
        Log.d(TAG, "onLocationChanged:"             +
                "  lat="       + loc.getLatitude()  +
                ", lon="       + loc.getLongitude() +
                ", timestamp=" + System.currentTimeMillis());
        // TODO: 22/01/2017 send to Firebase
    }

}
