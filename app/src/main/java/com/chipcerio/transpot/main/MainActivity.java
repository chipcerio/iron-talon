package com.chipcerio.transpot.main;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.chipcerio.transpot.R;
import com.chipcerio.transpot.signup.SignupActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/*
 * Before generating coordinates, it has to qualify 3 important this:
 *
 * 1. checks if GoogleApiClient is updated. if not, redirects to Google Play to update
 * 2. checks for runtime permission (Android 6.0) is granted. permission:ACCESS_FINE_LOCATION
 * 3. checks if GPS is enabled
 */
public class MainActivity extends AppCompatActivity implements ConnectionCallbacks,
        OnConnectionFailedListener, FirebaseAuth.AuthStateListener {

    private static final String TAG = "MainActivity";

    private static final int REQUEST_CHECK_SETTINGS = 100;

    private static final int REQUEST_FINE_LOCATION_PERMISSION = 101;

    private GoogleApiClient mGoogleApiClient;
    private int mPlayServicesStatusCode;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkPlayServices();

        Button button = (Button) findViewById(R.id.buttonStartService);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                locationRequest();
            }
        });

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        // if device has outdated Google Play Services,
        // check mGoogleApiClient for null to avoid crashing
        if (mGoogleApiClient == null) return;
        mGoogleApiClient.connect();

        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient == null) return;
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }

        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
        super.onStop();
    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user != null) {
            Log.d(TAG, "signedIn:" + user.getEmail());
        } else {
            Log.d(TAG, "signedOut");
            startActivity(new Intent(MainActivity.this, SignupActivity.class));
            finish();
        }
    }

    private void locationRequest() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            locationTask();
        } else {
            EasyPermissions.requestPermissions(this,
                    getString(R.string.text_app_needs_gps_enabled),
                    REQUEST_FINE_LOCATION_PERMISSION,
                    Manifest.permission.ACCESS_FINE_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @AfterPermissionGranted(REQUEST_FINE_LOCATION_PERMISSION)
    private void locationTask() {
        if (isGpsEnabled()) {
            Intent gpsService = new Intent(MainActivity.this, TranspotLocationService.class);
            startService(gpsService);
        } else {
            showUserGpsNotEnabledAlert();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int cause) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
    }

    private void showUserGpsNotEnabledAlert() {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.text_title_enable_gps))
                .setMessage(getString(R.string.text_message_enable_gps))
                .setPositiveButton(getString(R.string.button_gps_settings), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(settingsIntent, REQUEST_CHECK_SETTINGS);
                    }
                })
                .setNegativeButton(getString(R.string.button_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        dialogInterface.dismiss();
                    }
                })
                .create();
        dialog.show();
    }

    private boolean isGpsEnabled() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void showGooglePlayServicesErrorDialog(int statusCode) {
        GoogleApiAvailability googleApi = GoogleApiAvailability.getInstance();
        if (googleApi.isUserResolvableError(statusCode)) {
            googleApi.getErrorDialog(this, statusCode, 0).show();
        }
    }

    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleApi = GoogleApiAvailability.getInstance();
        int statusCode = googleApi.isGooglePlayServicesAvailable(this);
        switch (statusCode) {
            case ConnectionResult.SUCCESS:
                return true;

            default:
                mPlayServicesStatusCode = statusCode;
                return false;
        }
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void checkPlayServices() {
        if (isGooglePlayServicesAvailable()) {
            buildGoogleApiClient();
        } else {
            showGooglePlayServicesErrorDialog(mPlayServicesStatusCode);
        }
    }
}
